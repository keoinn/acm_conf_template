# Generic makefile for LaTeX: requires GNU make

TEXFILE = manuscript.tex
FIGFILE =

.PHONY: all dep clean

all:	$(TEXFILE:.tex=.pdf)

%.pdf: %.tex $(FIGFILE)
	( \
        pdflatex $<; \
        bibtex $(TEXFILE:.tex=); \
        pdflatex $<; \
        while grep -q "Rerun to get " $(<:.tex=.log); \
        do \
                pdflatex $<; \
        done \
        )

dep:

clean:
	@rm -f \
	$(TEXFILE:.tex=.pdf) \
	$(TEXFILE:.tex=.dvi) \
	$(TEXFILE:.tex=.aux) \
	$(TEXFILE:.tex=.out) \
	$(TEXFILE:.tex=.bbl) \
	$(TEXFILE:.tex=.blg) \
	$(TEXFILE:.tex=.spl) \
	$(TEXFILE:.tex=.log) \
	$(TEXFILE:.tex=.gz) \
	*.cut
